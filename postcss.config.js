const autoprefixer = require('autoprefixer');
const postcssimport = require('postcss-import');
const purgecss = require('@fullhuman/postcss-purgecss');
const tailwindcss = require('tailwindcss');

module.exports = {
    plugins: [
        postcssimport(),
        tailwindcss,
        autoprefixer,
        process.env.NODE_ENV === 'production'
            ? purgecss({
                content: [
                    './**/*.html',
                    './**/*.vue',
                ],
                defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
                whitelist: [
                    'button',
                    'input',
                    'textarea',
                    'dark:text-cgray-900',
                ],
                whitelistPatterns: [
                    /mapbox/,
                    /gu-/,
                ],
                whitelistPatternsChildren: [
                    /mapbox/,
                    /gu-/,
                ],
            })
            : '',
    ],
};
