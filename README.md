# mapview

## Setup

```bash
git clone https://gitlab.com/isaan/mapview-dev/mapview.git
```

## Dev

### Install

```bash
yarn install
```

### Run

```bash
yarn dev
```

### Tests

#### Lint

```bash
yarn test:lint
```

#### Unit

```bash
yarn test:unit
```

#### Unit with watcher

```bash
yarn test:unit
```

#### Lints and fixes files

```bash
yarn lint
```

## Production

### Automatic

```bash
bash scripts/deploy.sh
```

### Manual

```bash
# ...
yarn prod:build
yarn prod:start
```
