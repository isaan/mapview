// const favicons = require('favicons');
const faviconsConfig = require('./gulp/favicons.json')
const gulp = require('gulp')
const packageJson = require('./package.json')

faviconsConfig.appName = packageJson.name
faviconsConfig.appShortName = packageJson.name
faviconsConfig.appDescription = packageJson.description
faviconsConfig.developerName = packageJson.author.name
faviconsConfig.developerURL = packageJson.author.url
faviconsConfig.version = packageJson.version
faviconsConfig.url = packageJson.homepage

// const generateFavicons = () => {
//     return gulp.src('gulp/favicon.*')
//         .pipe(favicons.stream(faviconsConfig))
//         .pipe(gulp.dest(`public${faviconsConfig.path}`));
// };
//
// exports.default = gulp.series(
//     generateFavicons,
// );
