TABLE Database
Logiciel	Anydyn
VersionBDD	4
Locale	fr-FR
TimeZone	Paris, Madrid
TABLE Dossiers
Nom	Grau du roi
Client	Rincent NDT
Dossier	20-1295
Localite	LE GRAU DU ROI
Date	21/01/2020
Ouvrage	RD8
Commentaire	
TABLE PVs
Nom	PV01
Date	21/01/2021
Operateur	Sébastien Pellevrault
Voie	Lente
Sens	Vers Aix
PartieOuvrage	BAU
Climat	Soleil 6°C
Temperature	
Commentaire	Les essais sont conformes à une PF3 (>120 MPa)
TABLE ResultatsPV
Points_Nombre	8
PointsOK_Nombre	8
PointsOK_Max	146,6
PointsOK_Min	86,5
PointsOK_Moyenne	126,5
PointsOK_Std	18,5
PointsKOMin_Nombre	0
PointsKOMax_Nombre	0
PointsPass_Nombre	6
TABLE Plateformes
Type	Plateforme
Support	
Couche	Couche de forme
Materiau	Matériaux en place
GTR	N.S.
Classe	PF3
Seuil	120,00
TABLE ParamsPortance
Nom	Maxidyn Edyn2
AlgoRaideur	Dynaplaque
AlgoProcessing1	Boussinesq
AlgoProcessing2	Aucun
DPlaque	0,6
CPoisson	0,25
FForme	4
K	0
Alpha	0
MinPortance	20
MaxPortance	250
TABLE TypesResultatPortance
Nom	Module (MPa)
ResultStr	Module
ResultUnitStr	MPa
TABLE ParamsPoint
NbMiseEnPlace	2
NbMoyennage	1
NbTotal	3
TABLE Machines
Serial	MAX-117-D
Attribution	Rincent ND Technologies
MAC	98-F6-54-29-23-51-04-C2
LicStart	01/01/2017
LicEnd	01/01/2050
CertStart	20/02/2021
CertEnd	20/02/2022
TABLE ParamsAcqu
NbSamples	1500
FreqAcqu	30000,00
PreTrig	0,01
TABLE ReportPoints
Numero	1
Portance	123,3
PortanceAffichee	123
Qualite	92
Latitude	43,513832
Longitude	4,139671
DOP	4,8
Chainage	12
Commentaire	
Date	21/01/2021 10:01
TABLE Frappes
Numero	1
Portance	57,4
Qualite	95
Fmax	59,3
Umax	1614,5
PulseTime	18,0
TABLE Frappes
Numero	2
Portance	111,1
Qualite	92
Fmax	68,4
Umax	962,9
PulseTime	18,0
TABLE Frappes
Numero	3
Portance	123,3
Qualite	92
Fmax	68,1
Umax	938,7
PulseTime	18,0
TABLE ReportPoints
Numero	2
Portance	86,5
PortanceAffichee	86
Qualite	96
Latitude	43,513476
Longitude	4,139595
DOP	1,8
Chainage	268
Commentaire	
Date	21/01/2021 10:02
TABLE Frappes
Numero	1
Portance	33,6
Qualite	94
Fmax	55,1
Umax	2564,2
PulseTime	18,0
TABLE Frappes
Numero	2
Portance	82,7
Qualite	96
Fmax	67,2
Umax	1269,5
PulseTime	18,0
TABLE Frappes
Numero	3
Portance	86,5
Qualite	96
Fmax	67,6
Umax	1220,8
PulseTime	18,0
TABLE ReportPoints
Numero	3
Portance	119,8
PortanceAffichee	120
Qualite	94
Latitude	43,513140
Longitude	4,139505
DOP	1,8
Chainage	521
Commentaire	
Date	21/01/2021 10:03
TABLE Frappes
Numero	1
Portance	57,6
Qualite	84
Fmax	99,4
Umax	2698,4
PulseTime	18,0
TABLE Frappes
Numero	2
Portance	113,8
Qualite	94
Fmax	65,3
Umax	896,0
PulseTime	18,0
TABLE Frappes
Numero	3
Portance	0
Qualite	94
Fmax	64,8
Umax	844,7
PulseTime	18,0
TABLE ReportPoints
Numero	4
Portance	128,4
PortanceAffichee	128
Qualite	94
Latitude	43,512803
Longitude	4,139430
DOP	1,8
Chainage	753
Commentaire	
Date	21/01/2021 10:04
TABLE Frappes
Numero	1
Portance	61,8
Qualite	92
Fmax	74,3
Umax	1876,4
PulseTime	18,0
TABLE Frappes
Numero	2
Portance	123,5
Qualite	93
Fmax	68,5
Umax	866,5
PulseTime	18,0
TABLE Frappes
Numero	3
Portance	300
Qualite	94
Fmax	68,1
Umax	828,2
PulseTime	18,0
TABLE ReportPoints
Numero	5
Portance	128,4
PortanceAffichee	128
Qualite	95
Latitude	43,512409
Longitude	4,139325
DOP	1,5
Chainage	1024
Commentaire	
Date	21/01/2021 10:06
TABLE Frappes
Numero	1
Portance	77,3
Qualite	82
Fmax	104,3
Umax	2108,3
PulseTime	18,0
TABLE Frappes
Numero	2
Portance	96,0
Qualite	94
Fmax	69,5
Umax	1131,6
PulseTime	18,0
TABLE Frappes
Numero	3
Portance	128,4
Qualite	95
Fmax	68,1
Umax	981,3
PulseTime	18,0
TABLE ReportPoints
Numero	6
Portance	146,6
PortanceAffichee	147
Qualite	92
Latitude	43,511939
Longitude	4,139226
DOP	1,5
Chainage	1344
Commentaire	
Date	21/01/2021 10:07
TABLE Frappes
Numero	1
Portance	22,0
Qualite	88
Fmax	72,6
Umax	5152,3
PulseTime	18,0
TABLE Frappes
Numero	2
Portance	89,6
Qualite	92
Fmax	67,6
Umax	1178,1
PulseTime	18,0
TABLE Frappes
Numero	3
Portance	146,6
Qualite	92
Fmax	67,8
Umax	1096,5
PulseTime	18,0
TABLE ReportPoints
Numero	7
Portance	141,1
PortanceAffichee	141
Qualite	97
Latitude	43,512228
Longitude	4,138982
DOP	1,8
Chainage	1854
Commentaire	
Date	21/01/2021 10:09
TABLE Frappes
Numero	1
Portance	74,8
Qualite	87
Fmax	72,1
Umax	1506,9
PulseTime	18,0
TABLE Frappes
Numero	2
Portance	134,9
Qualite	97
Fmax	66,9
Umax	775,2
PulseTime	18,0
TABLE Frappes
Numero	3
Portance	141,1
Qualite	97
Fmax	67,1
Umax	743,1
PulseTime	18,0
TABLE ReportPoints
Numero	8
Portance	138,3
PortanceAffichee	138
Qualite	94
Latitude	43,512247
Longitude	4,138368
DOP	1,8
Chainage	2171
Commentaire	
Date	21/01/2021 10:10
TABLE Frappes
Numero	1
Portance	49,4
Qualite	89
Fmax	81,5
Umax	2576,1
PulseTime	18,0
TABLE Frappes
Numero	2
Portance	95,6
Qualite	95
Fmax	68,4
Umax	1118,6
PulseTime	18,0
TABLE Frappes
Numero	3
Portance	138,3
Qualite	94
Fmax	68,2
Umax	1083,4
PulseTime	18,0
	
	
