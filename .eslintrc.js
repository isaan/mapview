module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/essential',
        '@vue/airbnb',
        '@vue/typescript',
    ],
    rules: {
        'no-console': 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        indent: ['error', 4, { SwitchCase: 1 }],
        'import/prefer-default-export': 'off',
        'space-unary-ops': 'off',
        'no-else-return': 'off',
        'no-param-reassign': 'off',
        'no-underscore-dangle': 'off',
        'no-plusplus': 'off',
        'no-case-declarations': 'off',
        'class-methods-use-this': 'off',
        'import/extensions': 'off',
        'prefer-destructuring': 'off',
        'max-len': 'off'
    },
    parserOptions: {
        parser: '@typescript-eslint/parser',
    },
    overrides: [
        {
            files: [
                '**/__tests__/*.{j,t}s?(x)',
                '**/tests/unit/**/*.spec.{j,t}s?(x)',
            ],
            env: {
                jest: true,
            },
        },
    ],
};
