import * as Mapbox from 'mapbox-gl'
import * as Moment from 'moment'
import * as Numeral from 'numeral'
import * as JSZip from 'jszip'
import * as _ from 'lodash'
import 'numeral/locales'

import { DROPDOWN } from './data/constants'
import { Point } from './point'
import { Project } from './project'
import { ReportAnydyn } from './reportAnydyn'
import { Unit } from './unit'
import { Threshold } from './threshold'

export class Factory {
  public database: any

  public order: object[] = []

  public name: string = ''

  public state: string = 'import.chooseFile'

  private type: string = ''

  constructor() {
    Numeral.locale('fr')
    Moment.locale('fr')
  }

  // ---
  // PUBLIC
  // ---

  public async import(files: File[], project: Project) {
    if (files.length > 1) {
      this.state = 'import.tooManyFiles'
      return
    }

    // this.state = 'import.loading';
    const file = files[0]

    project.clean()

    let FILE_EXTENSION: string
    let FILE_NAME: string

    FILE_EXTENSION = file.name.split('.').pop() || ''
    this.name = file.name.slice(0, file.name.length - FILE_EXTENSION.length - 1)

    switch (FILE_EXTENSION) {
      case 'txt':
        const FILE_READER: FileReader = new FileReader()

        FILE_READER.onload = async () => {
          const RESULT: string = FILE_READER.result as string
          this.processTxt(project, RESULT)
        }

        FILE_READER.readAsText(file)

        break
      case 'dynz':
      case 'rptz':
        const JSZIP = new JSZip()
        JSZIP.loadAsync(file).then(
          (zip: any) => {
            Object.keys(zip.files).forEach((key: string) => {
              ;[FILE_NAME, FILE_EXTENSION] = key.split('.')
              switch (FILE_EXTENSION) {
                case 'txt':
                  if (!FILE_NAME.endsWith('InfosFile')) {
                    zip.files[key].async('string').then((content: any) => {
                      this.processTxt(project, content)
                    })
                  }
                  break
                case 'xlsx':
                case 'xls':
                  zip.files[key].async('arraybuffer').then((content: any) => {
                    const blob = new Blob([content], { type: 'text/plain' })
                    project.currentTemplate = {
                      name: key,
                      content: blob
                    }
                  })
                  break
                case 'png':
                case 'jpg':
                case 'jpeg':
                  zip.files[key].async('arraybuffer').then((content: any) => {
                    const blob = new Blob([content], { type: 'image/png' })

                    const reader = new FileReader()
                    reader.readAsDataURL(blob)
                    reader.onloadend = () => {
                      if (project.reports.current) {
                        project.reports.current.screenshots.push(reader.result as string)
                      }
                    }
                  })
                  break
                default:
                  break
              }
            })
          },
          () => {}
        )
        break
      default:
        break
    }
  }

  private processTxt(project: Project, content: string): void {
    try {
      if (project._mapview._map) {
        this.order = []
        this.convertDatabaseToObject(content)
        this.setProjectParameters(project._mapview._map, project)
        this.typeify(this.database, project)
        console.log('database', this.database)

        this.importDatabase(project._mapview._map, project)
        console.log('project', project)
        project.update()
        this.state = 'import.chooseFile'
      }
    } catch (e) {
      console.log(e)
      this.state = 'import.error'
    }
  }

  // ---
  // PRIVATE
  // ---

  private setProjectParameters(map: Mapbox.Map, project: Project): void {
    const REPORT: ReportAnydyn = new ReportAnydyn(
      map,
      this.database.TypesResultatPortance.ResultStr,
      this.database.TypesResultatPortance.ResultUnitStr,
      Number(this.database.ParamsPortance.MinPortance.replace(',', '.').replace(/\s/g, '')),
      Number(this.database.ParamsPortance.MaxPortance.replace(',', '.').replace(/\s/g, ''))
    )

    REPORT.values.current = undefined
    REPORT.values.list = ['Portance', 'Qualite', 'Fmax', 'Umax', 'PulseTime']
    if (!REPORT.values.current) {
      ;[REPORT.values.current] = REPORT.values.list
    }

    project.reports.current = REPORT
    project.reports.list.push(project.reports.current)

    REPORT.thresholds.list.forEach((threshold: Threshold) => {
      if (threshold.name === this.database.Plateformes.Classe) {
        REPORT.thresholds.current = threshold
      }
    })

    this.type = this.database.Database.Logiciel

    REPORT.machine = this.database.Machines.Serial.split('-')[0]
  }

  private convertDatabaseToObject(result: string): void {
    const LINES: string[] = result.split('\n')
    const OBJ: any = {}
    let index: string = ''

    // eslint-disable-next-line no-restricted-syntax
    for (const LINE of LINES) {
      let split: string[]

      if (LINE.includes('\t')) {
        split = LINE.trim().split('\t')
      } else {
        split = LINE.trim().split(' ')
      }

      if (split[0] === 'TABLE') {
        ;[, index] = split
        this.order.push({
          name: index,
          content: []
        })

        if (Array.isArray(OBJ[index])) {
          OBJ[index].push({})
        } else if (OBJ[index]) {
          OBJ[index] = [OBJ[index], {}]
        } else {
          OBJ[index] = {}
        }
      } else {
        ;(this.order[this.order.length - 1] as any).content.push(split[0])

        // if (RGX_NEG_DIST.test(split[0])) {
        //     split[0] = split[0].replace(/_0*/, '-')
        // } else if (RGX_POS_DIST.test(split[0])) {
        //     split[0] = split[0].replace(/D0*/, 'D')
        //     if (split[0].length === 1) {
        //         split[0] = 'D0'
        //     }
        // }

        if (!split[1]) {
          split[1] = ''
        }

        for (let index2 = 1; index2 < split.length; index2++) {
          const VALUE: any = split[index2]
          let obj: any

          if (Array.isArray(OBJ[index])) {
            obj = OBJ[index][Object.keys(OBJ[index]).length - 1]
          } else {
            obj = OBJ[index]
          }

          if (Array.isArray(obj[split[0]])) {
            obj[split[0]].push(VALUE)
          } else if (obj[split[0]]) {
            obj[split[0]] = [obj[split[0]], VALUE]
          } else {
            obj[split[0]] = VALUE
          }
        }
      }
    }

    console.log(OBJ)
    this.database = OBJ
  }

  // Heavydyn
  private static convertEntryToObject(
    report: ReportAnydyn,
    fieldName: string,
    fieldValue: string
  ): AdvancedType {
    const TO_NUMBER: (val: string) => number = val =>
      val === 'NaN' ? NaN : Numeral.default(val).value()

    switch (fieldName) {
      case 'Commentaire':
        return {
          kind: 'longString',
          value: fieldValue
        }
      case 'GTR':
      case 'ClassDeflection':
        return {
          kind: 'dropdownFixed',
          hidden: true,
          value: {
            current: fieldValue,
            list: DROPDOWN[fieldName]
          }
        }
      case 'Type':
      case 'Couche':
      case 'Materiau':
      case 'Etat':
        return {
          kind: 'dropdown',
          value: {
            current: fieldValue,
            list: DROPDOWN[fieldName]
          }
        }
      case 'Seuil':
        return {
          kind: 'unit',
          readonly: true,
          hidden: true,
          value: new Unit(TO_NUMBER(fieldValue), report.bearingCapacityUnitString, 2)
        }
      case 'Umax':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), 'um')
        }
      case 'Fmax':
      case 'Forcemax':
      case 'ValeurDrop':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), report.forceUnitSting)
        }
      case 'PulseTime':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), 'ms', 1)
        }
      case 'FreqAcqu':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), 'Hz', 2)
        }
      case 'Qualite':
        return {
          kind: 'number',
          value: fieldValue !== 'NaN' ? TO_NUMBER(fieldValue) : NaN
        }
      case 'Portance':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), report.bearingCapacityUnitString)
        }
      case 'Latitude':
      case 'Longitude':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), 'deg', 6)
        }
      case 'Chainage':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), 'm', 0)
        }
      case 'DOP':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), 'm')
        }
      case 'PulseTime':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), 's')
        }
      case 'PreTrig':
        return {
          kind: 'unit',
          value: new Unit(TO_NUMBER(fieldValue), 's', 2)
        }
      default:
        break
    }

    const RGX_BOOL: RegExp = /^((T|t)rue|(F|f)alse)$/
    const RGX_DATE: RegExp = /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}( [0-9]{2}:[0-9]{2})?$/
    // const RGX_DIST: RegExp = /^D-?[0-9]+$/
    // const RGX_FORC: RegExp = /^Force.*$/
    const RGX_NUM: RegExp = /^-?[0-9]+((,|\.| )[0-9]+)*$/
    // const RGX_TEMP: RegExp = /^T(air|man|surf)$/
    // const RGX_UNIT: RegExp = /^Unit.*$/
    const RGX_POINTS_OK: RegExp = /^PointsOK_((?!Nombre).)+$/
    const RGX_POINTS_KO: RegExp = /^PointsKO_((?!Nombre).)+$/

    switch (true) {
      case RGX_POINTS_OK.test(fieldName):
      case RGX_POINTS_KO.test(fieldName):
      // case RGX_DIST.test(fieldName): {
      //     return {
      //         kind: 'unit',
      //         value: new Unit(TO_NUMBER(fieldValue), report.distance),
      //     }
      // }
      case RGX_NUM.test(fieldValue): {
        return {
          kind: 'number',
          value: TO_NUMBER(fieldValue)
        }
      }
      // case RGX_FORC.test(fieldName): {
      //     return {
      //         kind: 'unit',
      //         value: new Unit(TO_NUMBER(fieldValue), report.force),
      //     }
      // }
      case RGX_BOOL.test(fieldValue): {
        if (fieldValue.toLowerCase() === 'true') {
          return {
            kind: 'boolean',
            hidden: true,
            value: true
          }
        } else {
          return {
            kind: 'boolean',
            hidden: true,
            value: false
          }
        }
      }
      case RGX_DATE.test(fieldValue): {
        // eslint-disable-next-line no-case-declarations
        let format: string = 'L'

        if (fieldValue.split(' ').length === 2) {
          format += ' hh:mm'
        }

        return {
          kind: 'date',
          readonly: true,
          oldValue: fieldValue,
          value: Moment.default(fieldValue, format)
        }
      }
      // case RGX_UNIT.test(fieldName): {
      //     return {
      //         kind: 'string',
      //         hidden: true,
      //         value: fieldValue,
      //     }
      // }
      default: {
        return {
          kind: 'string',
          value: fieldValue
        }
      }
    }
  }

  private importDatabase(map: Mapbox.Map, project: Project): void {
    project.information = this.database.Dossiers
    project.informationPlatform = this.database.Plateformes

    if (project.reports.current) {
      const report = project.reports.current

      report.information = this.database.PVs

      if (!this.database.ReportPoints.length) {
        this.database.ReportPoints = [this.database.ReportPoints]
      }

      this.database.ReportPoints.forEach((information: any) => {
        const nbDropPerPoint: number = this.database.ParamsPoint.NbTotal.value
        const nbDropToSkip: number = this.database.ParamsPoint.NbMiseEnPlace.value

        const index: number = nbDropPerPoint * report.points.children.length

        const drops: object[] = this.database.Frappes.slice(index, index + nbDropPerPoint)
        drops.forEach((drop: any) => {
          drop.NumeroReportPoints = information.Numero
        })

        const POINT = new Point(information, drops)
        POINT.setOriginalNumber(this.database.ReportPoints.indexOf(information) + 1)

        const valuesToUse = POINT.drops.slice(nbDropToSkip)

        POINT.values = {}

        const average = (a: number[]): any =>
          a.reduce((p: number, c: number): any => (c += p)) / a.length

        // console.log('valuesToUse', valuesToUse)
        if (valuesToUse.length > 0) {
          for (const key of Object.keys(valuesToUse[0])) {
            if (key) {
              const valueAvg: number = average(
                valuesToUse.map((values: any) => {
                  const value = (values as any)[key]
                  // console.log(key, value)
                  return value.kind === 'unit' ? value.value.toNumber() : value.value
                })
              )

              // console.log(valuesToAvg)

              POINT.values[key] =
                ((valuesToUse[0] as any)[key] as any).kind === 'unit'
                  ? {
                      kind: 'unit',
                      value: new Unit(
                        valueAvg,
                        ((valuesToUse[0] as any)[key] as any).value.toUnit(),
                        ((valuesToUse[0] as any)[key] as any).value.toPrecision()
                      )
                    }
                  : {
                      kind: 'number',
                      value: valueAvg
                    }
            }
          }
        }

        // console.log(POINT.values)

        report.points.addPoint(POINT)
      })

      report.addPointsToMap(map)
    }
  }

  private typeify(child: any, project: Project, name?: string): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const PROP in child) {
      if (typeof child[PROP] === 'string' && project.reports.current) {
        const report: ReportAnydyn = project.reports.current as ReportAnydyn
        child[PROP] = Factory.convertEntryToObject(report, name || PROP, child[PROP])
      } else if (Array.isArray(child[PROP])) {
        this.typeify(child[PROP], project, PROP)
      } else {
        this.typeify(child[PROP], project)
      }
    }
  }
}
