import { blend } from '../utils/blender';

const gray: string = blend('#ffffff', '#000000');

test('basic', () => {
    expect(blend('#ffffff', '#000000')).toBe(gray);
    expect(blend('#ffffff', '#ffffff')).toBe('#ffffff');
});

test('percentage', () => {
    expect(blend('#ffffff', '#000000', 0.5)).toBe(gray);
    expect(blend('#ffffff', '#000000', 0)).toBe('#ffffff');
    expect(blend('#ffffff', '#000000', 1)).toBe('#000000');
});

test('edge_cases', () => {
    expect(blend('#-1', '#000000')).toBe(gray);
    expect(blend('#gggggg', '#000000')).toBe(gray);
    expect(blend('#ffffff', '#000000', 1.5)).toBe(gray);
});
