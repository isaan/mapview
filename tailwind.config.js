const defaultTheme = require('tailwindcss/defaultTheme');
const tailwindUI = require('@tailwindcss/ui');

module.exports = {
    theme: {
        borderRadius: {
            none: '0',
            sm: '.125rem',
            default: '.25rem',
            lg: '.5rem',
            xl: '1rem',
            full: '9999px',
        },
        extend: {
            borderWidth: {
                3: '3px',
            },
            boxShadow: {
                outline: '0 0 0 3px rgba(125, 125, 125, 0.5)',
            },
            colors: {
                cgray: {
                    100: '#f5f5f5',
                    200: '#eeeeee',
                    300: '#e0e0e0',
                    400: '#bdbdbd',
                    500: '#9e9e9e',
                    600: '#757575',
                    700: '#616161',
                    800: '#424242',
                    900: '#212121',
                },
            },
            fontFamily: {
                sans: [
                    'Inter var',
                    ...defaultTheme.fontFamily.sans,
                ],
            },
            screens: {
                dark: {
                    raw: '(prefers-color-scheme: dark)',
                },
                light: {
                    raw: '(prefers-color-scheme: light)',
                },
            },
        },
    },
    variants: {
        borderColor: ['responsive', 'hover', 'focus', 'group-hover', 'active'],
        margin: ['responsive', 'first', 'last', 'not-first', 'not-last'],
        visibility: ['responsive', 'hover', 'focus', 'group-hover'],
    },
    plugins: [
        tailwindUI({
            // layout: 'sidebar',
        }),
        function f({ addVariant, e }) {
            const variants = [
                {
                    name: 'not-first',
                    css: 'not(:first-child)',
                },
                {
                    name: 'not-last',
                    css: 'not(:last-child)',
                },
            ];

            variants.forEach((variant) => {
                addVariant(variant.name, ({ modifySelectors, separator }) => {
                    modifySelectors(({ className }) => `.${e(`${variant.name}${separator}${className}`)}:${variant.css}`);
                });
            });
        },
    ],
};
